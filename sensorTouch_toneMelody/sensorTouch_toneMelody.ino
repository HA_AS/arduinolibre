#include "notesTone.h/notesTone.h"

//defino variables, para manejar cada pin
int led     = 13;
int touch   = 8;
int vocina  = 9;

// Aqui se define un vector, con las notas que harán parte de la melodia
int melody[] = {
  220, 523, 330, 880
};

// Duracion de las notas, 4 es igual a un cuarto.
int noteDurations[] = {
  4, 4, 4, 4
};

void setup() {
  //defino modo para los pines
  pinMode(led, OUTPUT);//entrada/salida
  pinMode(touch, INPUT);//entrada

}

void loop() {
  
  if(!digitalRead(touch))
  {
      //establesco indicador de luz, que cuando se pulse, encender led siempre
      digitalWrite(led,1);
      
     // Iteracion de las notas en la melodia (vector)
      for (int thisNote = 0; thisNote < 4; thisNote++) {
    
        // calcular la duracion de la nota, (cada segundo)
        // dividido, por el tipo de nota.
        // quarter note = 1000 / 4, eighth note = 1000/8
        int noteDuration = 1000 / noteDurations[thisNote];
        tone(vocina, melody[thisNote], noteDuration);
    
        //definir pausa entre notas
        int pauseBetweenNotes = noteDuration * 1.30;
        delay(pauseBetweenNotes);
        // detener reproduccion de tono
        noTone(vocina);
      }
   }
}
