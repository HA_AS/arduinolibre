//defino variables, para manejar cada pin
int led   = 13;
int touch = 8;
void setup() {
  //defino modo para los pines
  pinMode(led, OUTPUT);//entrada/salida
  pinMode(touch, INPUT);//entrada

}

void loop() {
  //establesco, que cuando se pulse, encender led
  digitalWrite(led,!digitalRead(touch));

}
